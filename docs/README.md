---
home: true
heroImage: /money-bills.jpg
tagline: Der Nettobetrag ist was man aus der Brutto-Arbeitslohn nach Steuern und Sozialabgaben in deine Konto bleibt
actionText: Netto →
actionLink: /rechner/
meta:
  - name: keyworkds
    content: wie viel netto rechner brutto lohn wieviel steuern bekomme
  - property: og:title
    content: Wie viel bekomme ich Netto?
  - property: og:description
    content: Der Nettobetrag ist was man aus der Brutto-Arbeitslohn nach Steuern und Sozialabgaben in deine Konto bleibt
  - property: og:url
    content: https://wievielnetto.de/
lang: de-DE
features:
- title: Kurzarbeit
  lang: de-DE
  details: Erfahre inwieweit deinen Gehalt betroffen ist.
- title: Welche Steuerklasse habe ich?
  details: Unter welche Bedingungen gehörst du zu einer Klasse oder sogar zu Mehreren.
- title: Nebenjob bekommen? Wie lautet ihre Netto?
  details: Erfahre die verschiedenen Möglichkeiten.

footer: wievielnetto.de
---
