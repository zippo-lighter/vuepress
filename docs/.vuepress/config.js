module.exports = {
    title: 'Wie viel Netto?',
    description: 'Der Nettobetrag ist was man aus der Brutto-Arbeitslohn nach Steuern und Sozialabgaben in deine Konto bleibt',
    base: '/',
    dest: 'public',
    themeConfig: {
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Rechner', link: '/rechner/' }
      ]
    },
    plugins: {
      'sitemap': {
        hostname: 'https://wievielnetto.de'
       },
      // '@vuepress/google-analytics':
      // {
      //   'ga': 'UA-169425667-1' // UA-00000000-0
      // }
    }
}
